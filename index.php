<?php
include_once("php/main.php");

template_top();
?>
<link rel="stylesheet" href="/css/index.css" />

<section class='screen01'>
	<div class='container'>
		<?=file_get_contents("img/sc01-back01.svg")?>
		<?=file_get_contents("img/sc01-back02.svg")?>
		<div class='row'>
			<div class='col-xs-offset-2 col-xs-8 text-center'>
				<img class='title' src='/img/index-header.svg' alt='ЧМ 2018 | ПРОВЕДЕМ ВМЕСТЕ' />
				<a href='/' class='btn01 c-pink' data-anchor='.screen05'> Присоедниться к проекту</a>
			</div>
		</div>
	</div>
</section>

<section class='screen02'>
	<div class='container'>
		<div class='row'>
			<div class='col-xs-12 text-center'>
				<h4 class='c-pink'>Привет!</h4>
				<p>В 2018 году наша страна впервые принимает Чемпионат Мира по футболу</p>
				<p>Мы уверены, что каждая спортивная команда может принять участие и внести свой вклад в глобальное служение своему городу, обществу, и стране. Мы разработали универсальный набор инструментов, благодаря которым Вы  и Ваша команда может эффективно послужить и понять, как можно идти пошагово в подготовке и проведении различного рода мероприятий «До, Во время и После» крупных событий.</p>
			</div>
		</div>
	</div>
</section>

<section class='screen03 c-white'>
	<div class='back' data-stellar-ratio="0.4"> </div>
	<div class='container'>
		<div class='row'>
			<div class='col-sm-10 col-sm-offset-1 text-center'>
				<h2><span class='txt01'>ЭФФЕКТИВНЫЕ</span></h2>
				<h2><span class='txt02'>ИНСТРУМЕНТЫ</span></h2>
				<h4><span class='txt03'>ДЛЯ СЛУЖЕНИЯ КАЖДОГО</span></h4>
				<p><span class='txt04'>Мы многие годы работаем и служим в мире  профессионального спорта и через спорт для разного уровня спортсменов. Мы видим, что любые, а тем более крупные события - это возможность для масштабного служения во всех городах нашей страны и за ее пределами.</span></p>
				<p><span class='txt05'>Мы постарались сделать так, чтобы каждый имел возможность и инструменты для служения.</span></p>
			</div>
		</div>
	</div>
</section>

<section class='screen04'>
	<div class='container'>
		<div class='row'>
			<div class='program col-sm-5'>
				<div class='program-head'>
					<h4 class='c-green'><span>СПОРТИВНЫЕ</h4>
					<h2 class='c-pink' ><span style='font-size: 1.2189em'>КЛУБЫ</h2>
				</div>
				<div class='program-body'>
					<p>Мероприятия или программы направленные на развитие физкультурно-оздоровительного и профессионального спорта в церкви, городе, стране.</p>
					<p>Цель: Воспитание учеников  и влияние на общество через спорт, укрепление морали и нравственности в обществе, содействие всестороннему и гармоничному воспитанию личности, развитие и совершенствование системы патриотического воспитания населения, продвижение христианской морали в среде профессионального спорта.</p>
				</div>
				<p class='program-btn'><a class='c-pink' onclick='setMaterials(2)' data-anchor='.screen12' href='/'>Хочу программу!</a></p>
			</div>
			<div class='program col-sm-5'>
				<div class='program-head'>
					<h4 class='c-green'><span>СПОРТИВНЫЕ</h4>
					<h2 class='c-pink' ><span style='font-size: 0.6957em'>ФЕСТИВАЛИ</h2>
				</div>
				<div class='program-body'>
					<p>Возможность построения личных отношений и развития через организацию досуга семей, детей, подростков, молодежи на общественной площадке города, используя существующие различные спортивные направления и возможности принять в них участие.</p>
					<p>Цель:  Помочь в восстановлении добрых отношений между родителями и детьми, построение дружественных отношений с людьми, популяризация христианского взгляда на здоровье, показать, как можно отдыхать весело, интересно и с пользой для тела и для души, презентация социальных и спортивных проектов церкви</p>
				</div>
				<p class='program-btn'><a class='c-pink' onclick='setMaterials(3)' data-anchor='.screen12' href='/'>Хочу программу!</a></p>
			</div>
		</div>
		<div class='row'>
			<div class='program col-sm-5'>
				<div class='program-head'>
					<h4 class='c-green'><span>СПОРТИВНЫЕ</h4>
					<h2 class='c-pink' ><span style='font-size: 0.9015em'>ТУРНИРЫ</h2>
				</div>
				<div class='program-body'>
					<p>Объединение религиозных и различных организаций, для участия в турнире по различным видам спорта.</p>
					<p>Цель: Объединить на одной площадке, разносторонние спортивные команды, для построения отношений и новых знакомств.</p>
				</div>
				<p class='program-btn'><a class='c-pink' onclick='setMaterials(1)' data-anchor='.screen12' href='/'>Хочу программу!</a></p>
			</div>
			<div class='program col-sm-5'>
				<div class='program-head'>
					<h4 class='c-green'><span>СПОРТИВНЫЕ</h4>
					<h2 class='c-pink' ><span style='font-size: 1.1616em'>ЛАГЕРЯ</h2>
				</div>
				<div class='program-body'>
					<p>Место для проведения спортивных сборов, а также укрепления и возрастания профессиональных навыков юных спортсменов.</p>
					<p>Цель: Показать, как спортивные сборы влияют на подготовку, как физического здоровья, так и духовного воспитания спортсменов.</p>
				</div>
				<p class='program-btn'><a class='c-pink' onclick='setMaterials(4)' data-anchor='.screen12' href='/'>Хочу программу!</a></p>
			</div>
		</div>
	</div>
</section>

<section class='screen05 c-white'>
	<div class='back' data-stellar-ratio="0.4"> </div>
	<div class='container'>
		<div class='row'>
			<div class='col-sm-10 col-sm-offset-1 text-center'>
				<h2><span class='txt01'>ВСЕ ОЧЕНЬ</span></h2>
				<h2><span class='txt02'>ПРОСТО</span></h2>
				<p><span class='txt03'>Программы по направлениям максимально простые и сделаны таким образом, чтобы каждый желающий мог реализовать любой проект для своего города.</span></p>
				<p><span class='txt04'>Для этого Вам нужно сделать несколько шагов.</span></p>
			</div>
		</div>
	</div>
</section>

<section class='screen06'>
	<div class='container'>
		<div class='row'>
			<div class='col-sm-3 step'>
				<h1 class='step-head'>
					<span class='c-green'>1</span>
				</h1>
				<p class='step-body'>
					<span>Скачайте нужную программу</span>
				</p>
			</div>
			<div class='col-sm-3 col-sm-offset-1 step'>
				<h1 class='step-head'>
					<span class='c-green'>2</span>
				</h1>
				<p class='step-body'>
					<span>Следуйте программе и применяйте «до, во время и после» крупных спортивных событий</span>
				</p>
			</div>
			<div class='col-sm-3 col-sm-offset-1 step'>
				<h1 class='step-head'>
					<span class='c-green'>3</span>
				</h1>
				<p class='step-body'>
					<span>Подготовьте отчет о реализации мероприятия по форме на сайте</span>
				</p>
			</div>
		</div>
	</div>
</section>

<section class='screen07 c-white'>
	<div class='back' data-stellar-ratio="0.4"> </div>
	<div class='container'>
		<div class='row'>
			<div class='col-sm-10 col-sm-offset-1 text-center'>
				<h2><span class='txt01'>ВЫБИРАЙТЕ</span></h2>
				<h1><span class='txt02'>ВАШУ</span></h1>
				<h2><span class='txt03'>ПРОГРАММУ</span></h2>
			</div>
		</div>
	</div>
</section>

<section class='screen08'>
	<div class='container'>
		<div class='row text-center'>
			<p>Вы можете скачивать и пользоваться любой программой или всеми сразу. Это бесплатно. У нас только одно условие - после проведения мероприятий по  программе, предоставьте отчет по простой форме.</p>
		</div>
		<div class='row products'>
			<a class='product' onclick='setMaterials(3)' data-anchor='.screen12' href='/' style='background-image: url(/img/program1.jfif)'><p class='c-pink'>Получить программу</p></a>
			<a class='product' onclick='setMaterials(1)' data-anchor='.screen12' href='/' style='background-image: url(/img/program2.jfif)'><p class='c-pink'>Получить программу</p></a>
			<a class='product' onclick='setMaterials(4)' data-anchor='.screen12' href='/' style='background-image: url(/img/program3.jfif)'><p class='c-pink'>Получить программу</p></a>
			<a class='product' onclick='setMaterials(2)' data-anchor='.screen12' href='/' style='background-image: url(/img/program4.jfif)'><p class='c-pink'>Получить программу</p></a>
		</div>
		<div class='row text-center'>
			<p>Мы также рекомендуем Вам использовать пакет фирменных материалов и брендбук «На старт, внимание, марш!»</p>
		</div>
		<div class='row products'>
			<a class='product'  onclick='setMaterials(5)' data-anchor='.screen12' href='/' style='background-image: url(/img/guidlines.jfif)'><p class='c-pink'>Получить</p></a>
			<a class='product'  onclick='setMaterials(6)' data-anchor='.screen12' href='/' style='background-image: url(/img/mk.jfif)'><p class='c-pink'>Получить</p></a>
		</div>
	</div>
</section>

<section class='screen11'>
	<div class='back' data-stellar-ratio="0.4"> </div>
</section>

<section class='screen12'>
	<div class='container'>
		<div class='col-sm-8 col-sm-offset-2 text-center'>
			<div class='row'>
				<h2><span class='txt01 c-green'>ПОЛУЧИТЕ</span></h2>
				<h2><span class='txt02 c-pink'>МАТЕРИАЛЫ</span></h2>
				<p class='txt03'>Заполните форму, укажите Ф.И.О, адрес для отправки материалов и необходимое количество ресурсов.</p>
				<form class='form1' action='/form.php' method='POST'>
					<input type='hidden' name='formname' value='get_materials'>
					<input name='name' placeholder='Ваше имя' required />
					<input name='city' placeholder='Город' required />
					<input name='email' type='email' placeholder='E-mail' required />
					<input name='phone' placeholder='Контактный телефон' required />
					<!--div data-select-wrapper='material_type' data-required data-placeholder="- Тип материалов -" tabindex='0'>
						<div class='options'>
							<span data-value='1'>Программы «На старт, внимание, марш!»</span>
							<span data-value='2'>Ресурсы для служения</span>
						</div>
					</div>
					<div data-select-wrapper='program' style='display: none' data-placeholder="- Программа -" tabindex='0'>
						<div class='options'>
							<span data-value='1'>Спортивные турниры</span>
							<span data-value='2'>Спортивные клубы</span>
							<span data-value='3'>Спортивные фестивали</span>
							<span data-value='4'>Спортивные лагеря</span>
						</div>
					</div>
					<div data-select-wrapper='resource' style='display: none' data-placeholder="- Ресурс -" tabindex='0'>
						<div class='options'>
							<span data-value='1'>Счастье есть</span>
							<span data-value='2'>Он победитель</span>
							<span data-value='3'>Окончательнся победа</span>
							<span data-value='4'>Арена</span>
							<span data-value='5'>Игрок</span>
							<span data-value='6'>Кубок</span>
						</div>
					</div>
					<div class='resource_count_wrapper' style='display: none'>
						<input name='resource_count' placeholder='Количество ресурсов' />
					</div-->
					<p>Какие материалы вы желаете получить?</p>
					<label><input type='checkbox' name='materials[]' value='1'> Программа "Спортивные турниры"</label>
					<label><input type='checkbox' name='materials[]' value='2'> Программа "Спортивные клубы"</label>
					<label><input type='checkbox' name='materials[]' value='3'> Программа "Спортивные фестивали"</label>
					<label><input type='checkbox' name='materials[]' value='4'> Программа "Спортивные лагеря"</label>
					<label><input type='checkbox' name='materials[]' value='5'> Руководство по фирменному стилю</label>
					<label><input type='checkbox' name='materials[]' value='6'> Маркетинг-кит "На старт, внимание, марш!"</label>

					<textarea name='text' placeholder='Напишите ваше письмо...'></textarea>
					<button class='c-green'>Отправить</button>
				</form>
			</div>
		</div>
	</div>
</section>

<?php
template_bottom()
?>

<script src='/js/index.js'></script>