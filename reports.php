<?php
include_once("php/main.php");

template_top();
?>
<link rel="stylesheet" href="/css/reports.css" />

<section class='screen01'>
	<div class='container'>
		<?=file_get_contents("img/sc01-back01.svg")?>
		<?=file_get_contents("img/sc01-back02.svg")?>
		<div class='row'>
			<div class='col-xs-offset-2 col-xs-8 text-center'>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 728 413.32" style='overflow: visible;'>
					<text style='font-size:128.5px;fill:#c3006f;font-family:Gilroy-ExtraBold;font-weight:700;' transform="translate(4.71 111.29)">
						ВАШ ОТЧЕТ
					</text>
					<text style='font-family:Gilroy-ExtraBold;font-weight:700;font-size:212px;fill:#a0be37;' transform="translate(0 307.15)">
						ВАЖЕН
					</text>
					<text style='font-family:Gilroy-ExtraBold;font-weight:700;font-size:55.2px;fill:#e3a631;' transform="translate(4.71 398.2)">
						ДЛЯ РАЗВИТИЯ ДВИЖЕНИЯ
					</text>
				</svg>
			</div>
		</div>
	</div>
</section>

<section class='screen02'>
	<div class='container'>
		<div class='row'>
			<div class='col-sm-10 col-sm-offset-1 text-center'>
				<h4 class='c-pink'>Зачем заполнять отчет?</h4>
				<p>Ваши отчеты мотивируют нас и тысячи других людей присоединяться к служению через спорт. Благодаря вашим отчетам мы можем понять, что в программах необходимо улучшить и модернизировать.</p>
			</div>
		</div>
	</div>
</section>


<section class='screen03 c-white'>
	<div class='back' data-stellar-ratio="0.4"> </div>
	<div class='container'>
		<div class='row'>
			<div class='col-sm-10 col-sm-offset-1 text-center'>
				<h2><span class='txt01'>ХОРОШИЙ</span></h2>
				<h2><span class='txt02'>ПРИЗ</span></h2>
				<h2><span class='txt03'>ЗА ХОРОШИЙ ОТЧЕТ</span></h2>
			</div>
		</div>
	</div>
</section>

<section class='screen05'>
	<div class='container'>
		<div class='row'>
			<div class='col-sm-10 col-sm-offset-1 text-center'>
				<h4 class='c-pink'>Мы хотим подарить подарки!</h4>
				<p>Наша команда внимательно изучает каждый присланный отчет, если вы проделали большую работу, мы обязательно хотим подарить подарки вашей команде. Заполните простую форму отчетности и вы станете участником конкурса. </p>
				<form class='form1' action='/form.php' method='POST'>
					<input type='hidden' name='formname' value='report' />
					<input name='city' placeholder='Ваш город' required />
					<input name='FIO' placeholder='Имя, фамилия' required />
					<input name='email' type='email' placeholder='Ваш e-mail' required />
					<input name='phone' type='tel' placeholder='Контактный телефон' required />
					<input name='currency' placeholder='Какое направление вы использовали?' required />
					<input name='count' placeholder='Сколько человек приняло участие?' required />
					<textarea name='report' placeholder='Напишите краткое свидетельство после организации мероприятий' required ></textarea>
					<textarea name='plans' placeholder='Как вы планируете выполнять дальнейшую работу?' required ></textarea>
					<!--p>Прикрепите до 10 фотографий, описывающих ваши мероприятия.</p>
					<div class='uploader'>
						<div class='uploaded' style='background-image: url(/img/uploaded.jpg);'> </div>
						<div class='uploaded' style='background-image: url(/img/uploaded.jpg);'> </div>
						<div class='uploaded' style='background-image: url(/img/uploaded.jpg);'> </div>
						<div class='uploaded' style='background-image: url(/img/uploaded.jpg);'> </div>
						<div class='input'><input type='file' /></div>
					</div-->
					<button class='c-green'>Отправить</button>
				</form>
			</div>
		</div>
	</div>
</section>

<?php
template_bottom()
?>

<script src='/js/reports.js'></script>