<?php
include_once("php/main.php");
include_once("php/Socials.php");

template_top();

$statuses = [
	0 => "На модерации",
	1 => "Подтверждённое",
	2 => "неподтверждённое"
]
?>
<link rel="stylesheet" href="/css/liveManager.css" />
<link rel="stylesheet" href="/css/jquery.fancybox.min.css" />

<section>
	<div class='container'>
		<div class='row'>
			<div class='col-xs-12'>
				<button onclick='update.call(this)'>Загрузить новые посты</button>
			</div>
			<div class='col-xs-12'>
				<?=array_reduce($statuses, function($last, $next){static $i = 0; return $last . "<label><input type='checkbox' class='showType' value='".($i++)."' /> $next</label><br />";}, "")?>
			</div>
			<?php
			$posts = Socials::getMany();
			foreach($posts as $post)
			{
				?>
				<div class='col-xs-12 post status<?=$post->status?>' style='display: none;'>
					<a href='<?=$post->image?>' data-fancybox="group<?=$post->status?>"><img src='<?=$post->image?>' alt='' /></a>
					<h4><a href='<?=$post->author_url?>' target='_blank'><?=$post->username?></a></h4>
					<p><?=$post->info?></p>
					<select class='stateChanger' data-ai='<?=$post->ai?>'>
						<?=array_reduce($statuses, function($last, $next)use($post){static $i = 0; return $last . "<option value='".($i)."' ".($post->status==$i++ ? "selected" : "")." />$next</option>";}, "")?>
					</select>
				</div>
				<?php
			}
			?>
		</div>
	</div>
</section>

<?php
template_bottom()
?>

<script src='/js/liveManager.js'></script>
<script src='/js/jquery.fancybox.min.js'></script>