<?php
include("php/main.php");
include("php/PHPMailer/autoloader.php");

function post($href, $data)
{
	return file_get_contents($href, false, stream_context_create([
		'http' => [
			'method'  => 'POST',
			'content' => http_build_query($data)
		]
	]));
}

function replace($args, $str)
{
	for($i = count($args) - 1; $i >= 0; --$i){
		$str = str_replace('$' . ($i + 1), $args[$i], $str);
	}
	return $str;
}

$attr = [
	// Таблица
	"t" => "width='100%' style='width: 100%; border-collapse: collapse; table-layout: fixed; margin: 0 auto; border: 1px solid rgba(0,0,0,0.5);'",
	// Ячейка заголовка
	"h" => "width='20%' style='background-color: #DDDDDD; padding: 15px; border: 1px solid rgba(0,0,0,0.5);'",
	// Ячейка значения
	"b" => "style='padding: 15px; border: 1px solid rgba(0,0,0,0.5);' align='left'",
	// Ссылка
	"a" => "style='color: black!important; text-decoration: none; border-bottom: 1px solid rgba(0,0,0,0.5); padding-bottom: 2px;' target='_blank'",
];

$fields =
[
	"get_materials" =>
	[
		"theme" => "НАШЧМ.РФ | Заявка на материалы",
		"fields" =>
		[
			"name" => [
				"placeholder" => "Имя",
				"required" => true,
			],
			"city" => [
				"placeholder" => "Город",
				"required" => true,
			],
			"email" => [
				"type" => "email",
				"placeholder" => "E-mail",
				"required" => true,
			],
			"phone" => [
				"type" => "tel",
				"placeholder" => "Телефон",
				"required" => true,
			],
			"materials" => [
				"type" => "array",
				"placeholder" => "Материалы",
				"verify" => function(Array $arr)
				{
					$ret = [];
					if(count($arr) == 0) throw new Exception("Пожалуйста, выберите хотя бы один материал, который вы хотели бы получить");
					foreach($arr as &$val)
					{
						switch($val)
						{
						case 1: $ret[$val] = 'Программа "Спортивные турниры"'; break;
						case 2: $ret[$val] = 'Программа "Спортивные клубы"'; break;
						case 3: $ret[$val] = 'Программа "Спортивные фестивали"'; break;
						case 4: $ret[$val] = 'Программа "Спортивные лагеря"'; break;
						case 5: $ret[$val] = 'Руководство по фирменному стилю'; break;
						case 6: $ret[$val] = 'Маркетинг-кит "На старт, внимание, марш!"'; break;
						}
					}
					return $ret;
				},
				"work" => function(Array $values)
				{
					return implode("<br />", $values);
				}
			],
			"text" => [
				"type" => "textarea",
				"placeholder" => "Сообщение"
			],
		],
		"autosend" =>
		[
			"template" => function($form)
			{
				ob_start();
				?>
				<p>Добрый день! Большое спасибо за ваш интерес к программам "На старт, внимание, марш!"<br />Мы верим, что эти инструменты помогут вам в служении.<br />Ваши материалы доступны по ссылкам:</p>
				<p>
					<?php
					foreach($form["materials"] as $i => $name)
					{
						?>
						<a href='<?=$_SERVER["HTTP_REFERER"]?>files/material-<?=$i?>.zip' target='_blank'><?=$name?></a><br />
						<?php
					}
					?>
				</p>
				<p>Мы просим вас после использования материалов, подготовить отчет<br />на нашем сайте. Для отправки отчета вы можете воспользоваться этой <a href='<?=$_SERVER["HTTP_REFERER"]?>reports.php' target='_blank'>ссылкой</a>.</p>
				<?php
				$data = ob_get_contents();
				ob_end_clean();
				return $data;
			},
			"address" => function($form)
			{
				return $form["email"];
			}
		]
	],
	"report" =>
	[
		"theme" => "НАШЧМ.РФ | Отчёт",
		"fields" =>
		[
			"city" => [
				"placeholder" => "Город",
				"required" => true,
			],
			"FIO" => [
				"placeholder" => "ФИО",
				"required" => true,
			],
			"email" => [
				"type" => "email",
				"placeholder" => "E-mail",
				"required" => true,
			],
			"phone" => [
				"type" => "tel",
				"placeholder" => "Контактный телефон",
				"required" => true,
			],
			"currency" => [
				"placeholder" => "Направление",
				"required" => true,
			],
			"count" => [
				"placeholder" => "Количество участников",
				"required" => true,
			],
			"report" => [
				"type" => "textarea",
				"placeholder" => "Текст отчёта"
			],
			"plans" => [
				"type" => "textarea",
				"placeholder" => "План дальнейшей работы"
			],
		]
	],
	"contact" =>
	[
		"theme" => "НАШЧМ.РФ | Обратная связь",
		"fields" =>
		[
			"FIO" => [
				"placeholder" => "Имя",
				"required" => true,
			],
			"address" => [
				"placeholder" => "Город",
			],
			"email" => [
				"type" => "email",
				"placeholder" => "E-mail",
				"required" => true,
			],
			"phone" => [
				"type" => "tel",
				"placeholder" => "Контактный телефон",
			],
			"report" => [
				"type" => "textarea",
				"placeholder" => "Дополнительная информаци"
			],
		]
	],
];

if(isset($_GET["report"]))
{
	if(!( $form = $fields[$_GET["report"]] ))
	{
		header( $_SERVER['SERVER_PROTOCOL']." 404 Not Found", true );
		echo "<h1>404 Not Found</h1>";
		die();
	}
	$d = ';';

	header("Content-Type: text/csv");
	header("Content-Disposition: inline; filename={$_GET["report"]}.csv");
	echo chr(239) . chr(187) . chr(191);

	foreach($form["fields"] as $field)
	{
		echo $d . '"' . str_replace('"', '""', $field["placeholder"]) . '"';
	}

	$values = App::db("SELECT * FROM `forms` WHERE `form` = '".App::db()->real_escape_string($_GET["report"])."' ORDER BY `date` DESC");
	while($next = $values->fetch_assoc())
	{
		echo "\n" . $next["date"];
		$next = json_decode($next["data"], true);
		foreach($form["fields"] as $name => $field) {
			if (is_array($next[$name])) {
				$next[$name] = implode("\n", $next[$name]);
			}
			echo $d . '"' . str_replace('"', '""', $next[$name]) . '"';
		}
	}
	exit(0);
}

try{
	if(!($form = $fields[$_POST["formname"]]))
	{
		throw new Exception(replace([$_POST["formname"]], "Форма \"$1\" не найдена"));
	}

	/*
	$grecaptcha = json_decode(post("https://www.google.com/recaptcha/api/siteverify", [
		"secret" => "",
		"response" => $_POST["grecaptcha-response"],
		"remoteip" => $_SERVER['REMOTE_ADDR']
	]));
	
	if(!$grecaptcha->success)
	{
		throw new Exception("Google recaptcha failed \"".print_r($grecaptcha->{"error-codes"}, 1)."\"");
	}
	*/

	$mailer = new PHPMailer();  
	$mailer->IsMAIL();                 
	$mailer->CharSet = "UTF-8";

	$mailer->setFrom("info@нашчм.рф", "НАШЧМ");
	$mailer->addAddress("Wcrus18@gmail.com");
	$mailer->addBCC("orehov19@gmail.com");

	$mailer->isHTML(true);                             

	$mailer->Subject = ($form[theme]);
	$mailer->Body = '<h3>От пользователя были получены следующие данные</h3><table '.$attr["t"].'>';
	$fields = [];
	
	foreach($form["fields"] as $name => $field)
	{
		$field = array_merge([
			"type" => "text",
			"placeholder" => $name,
			"required" => false,
		], $field);
		
		switch($field["type"])
		{
		case "array":
				if(!isset($_POST[$name]) || !is_array($_POST[$name]))
					throw new Exception(replace([$field["placeholder"]], "Поле \"$1\" не заполнено"));
				$value = $field["verify"]($_POST[$name]);
			break;
		default:
			$value = "";
			if(isset($_POST[$name])) $value = htmlspecialchars($_POST[$name]);
			if(empty($value))
			{
				if($field["required"]) 
					throw new Exception(replace([$field["placeholder"]], "Поле \"$1\" не заполнено"));
				else
					continue;
			}
		}

		$fields[$name] = $value;
		
		switch($field["type"])
		{
		case "text":
			$mailer->Body .= "<tr><td $attr[h] valign='right'>$field[placeholder]:</td><td $attr[b]>$value</td></tr>";
			break;

		case "textarea":
			$mailer->Body .= "<tr><td $attr[h] valign='center' colspan='2'>$field[placeholder]:</td></tr><tr><td colspan='2' $attr[b]>".str_replace("\n", '<br />', $value)."</td></tr>";
			break;

		case "email":
			$mailer->Body .= "<tr><td $attr[h] valign='right'>$field[placeholder]:</td><td $attr[b]><a $attr[a] href='mailto:$value'>$value</a></td></tr>";
			break;

		case "tel":
			$mailer->Body .= "<tr><td $attr[h] valign='right'>$field[placeholder]:</td><td $attr[b]><a $attr[a] href='tel:$value'>$value</a></td></tr>";
			break;
		case "array":
			$mailer->Body .= "<tr><td $attr[h] valign='right'>$field[placeholder]:</td><td $attr[b]>".$field["work"]($value)."</td></tr>";
			break;
		}
	}
	$mailer->Body .= '</table>';

	App::db("INSERT INTO `forms` SET `form` = '".App::db()->real_escape_string($_POST["formname"])."', `data` = '".App::db()->real_escape_string(json_encode($fields))."'");

	if(!$mailer->send()) {
		@mail("romash1408@yandex.ru", "НАШЧМ.РФ | Sending error", "<p>User data:</p><pre>".print_r($fields, 1)."</pre><p>Error message: <b>".$mailer->ErrorInfo."</b> when sending report</p>");
	}

	if(!($autosend = $form["autosend"]))
	{
		die(json_encode(["success" => 1, "act" => [
			"reset" => true,
			"close" => true,
			"alert" => "Данные успешно отправлены"
		]]));
	}

	$mailer->clearAddresses();
	$mailer->addAddress($autosendGetter = $autosend["address"]($fields));
	$mailer->Body = $autosend["template"]($fields);

	if(!$mailer->send()) {

		@mail("romash1408@yandex.ru", "НАШЧМ.РФ | Sending error", "<p>Autosend to $autosendGetter doesn't work</p><p>Error message: <b>".$mailer->ErrorInfo."</b></p>");

		die(json_encode(["success" => 0, "message" => "Нам не удалось автоматически отправить ответ на указанный вами электронный адрес. Все введённые вами данные были сохранены и в ближайшее время будут обработаны нашими специалистами. Просим прощения за предоставленные неудобства. 🐈"]));

	} else {
		die(json_encode(["success" => 1, "act" => [
			"reset" => true,
			"close" => true,
			"alert" => "Данные успешно отправлены"
		]]));
	}
}
catch(Exception $e)
{
	die(json_encode(["success" => 0, "message" => $e->getMessage()]));
}
catch(phpmailerException $e)
{
	die(json_encode(["success" => 0, "message" => $e->getMessage()]));
}
?>