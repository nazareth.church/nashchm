<?php
include_once("SimpleApp.php");
include_once("Socials.php");

const MAX_POSTS_COUNT = 8;

if(isset($_POST["getContent"]))
{
	$last = null;
	if(isset($_POST["lastPost"]))
	{
		$last = $_POST["lastPost"];
	}

	$data = 
		[
			"success" => 1,
			"posts" => Socials::getMany(MAX_POSTS_COUNT, Socials::STATUS_CONFIRMED, $last, $last, $_SERVER['REMOTE_ADDR']),
			"last" => $last
		];
		
	die(json_encode($data));
}

if(isset($_POST["like"]))
{
	$post = Socials::get(["ai" => $_POST["like"] * 1]);
	if(!$post) die('{"success" => 0, "message" => "Пост #'.$_POST["like"].' не найден."');
	$post->like("toggle", $_SERVER['REMOTE_ADDR']);
	die(json_encode([
		"success" => 1,
		"likes" => $post->like(),
		"liked" => $post->like("get", $_SERVER['REMOTE_ADDR'])
	]));
}

if(isset($_POST["updatePost"]))
{
	if(!($post = Socials::get(["ai" => $_POST["updatePost"]]))) die('{"success": 0, "message": '.App::db()->error.'"}');
	$post->update(["status" => $_POST["value"]]);
	die('{"success": 1}');
}

if(isset($_POST["update"]))
{
	if(!($tags = $config["hashTag"])) die('{"success": 0, "message": "Не указан hashTag в конфиг файле"');
	if(!is_array($tags)) $tags = [$tags];
	foreach($tags as $tag)
	{
		$instContent = file_get_contents("https://www.instagram.com/explore/tags/$tag/?__a=1");
		if(!$instContent) die('{"success": 0, "message": "Ошибка при обращении к instagram (https://www.instagram.com/explore/tags/$tag/?__a=1)"');
		if(!($inst = json_decode($instContent))) die('{"success": 0, "message": "Ответ от instagram не может быть обработан: '.$instContent.'"');
		if(!($posts = $inst->graphql->hashtag->edge_hashtag_to_media->edges)) die('{"success": 0, "message": "Не найден контент в ответе от Instagram: <pre>'.print_r($inst, true).'</pre>"');
		
		foreach($posts as $post)
		{
			$post = $post->node;
			if($post->is_video) continue;
			if(Socials::get(["id" => $post->shortcode, "type" => "inst"])) continue;
			
			if(!($infoContent = @file_get_contents($req = "https://www.instagram.com/p/{$post->shortcode}/?tagged=$tag&__a=1")))
			die('{"success": 0, "message": "Ошибка при обращении к instagram ('.$req.')"');
			if(!($info = json_decode($infoContent))) die('{"success": 0, "message": "Ответ от instagram не может быть обработан: '.$infoContent.'"');
			if(!($postInfo = $info->graphql->shortcode_media)) die('{"success": 0, "message": "Не найден контент в ответе от Instagram: <pre>'.print_r($info, true).'</pre>"');
			
			Socials::add([
				"id" => $post->shortcode,
				"type" => "inst",
				"image" => $post->thumbnail_src,
				"info" => $post->caption,
				"username" => $postInfo->owner->username,
				"date" => [
					"type" => "processed",
					"value" => "= FROM_UNIXTIME({$postInfo->taken_at_timestamp})"
				],
				"author_url" => "https://www.instagram.com/" . $postInfo->owner->username, "technical" => ""
			]);
		}

		$answer = json_decode(file_get_contents("https://api.vk.com/method/newsfeed.search?q=%23$tag&access_token=65fc593d65fc593d65fc593d9c65a00f29665fc65fc593d3cd7ef8bf7692547a184d133&start_time=1483288146&v=5.65&extended=1"))->response;

		$profInfo = [];
		foreach($answer->profiles as $prof)
		{
			$profInfo[$prof->id] = [
				"name" => $prof->first_name . " " . $prof_last_name,
				"nick" => $prof->screen_name,
				"url" => "https://vk.com/" . $prof->screen_name
			];
		}
		foreach($answer->groups as $group)
		{
			$profInfo[-$group->id] = [
				"name" => $group->name,
				"nick" => $group->screen_name,
				"url" => "https://vk.com/" . $group->screen_name
			];
		}

		foreach($answer->items as $post)
		{
			if(!$post->attachments) continue;
			if(Socials::get(["id" => $post->id, "type" => "vk"])) continue;
			foreach($post->attachments as $attach)
			{
				if($attach->type == "album")
				{
					$attach->type = "photo";
					$attach->photo = $attach->thumb;
				}

				if($attach->type != 'photo') continue;

				Socials::add([
					"id" => $post->id,
					"type" => "vk",
					"image" => $attach->photo->photo_604,
					"info" => $post->text,
					"username" => $profInfo[$post->from_id]["name"],
					"date" => [
						"type" => "processed",
						"value" => "= FROM_UNIXTIME({$post->date})"
					],
					"author_url" => $profInfo[$post->from_id]["url"], "technical" => ""
				]);
			}
		}
	}
	
	die('{"success": 1}');
}
?>