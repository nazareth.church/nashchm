<?php
class Instagram{
	public
		$authorized;
	
	public function __construct()
	{
		$this->authorized = false;
	}
	
	public function isAuthorized()
	{
		return $this->authorized;
	}
}

function post($href, $data)
{
	return file_get_contents($href, false, stream_context_create([
		'http' => [
			'method'  => 'POST',
			'content' => http_build_query($data)
		]
	]));
}

$tag = $_GET["tag"];
$answer = json_decode(file_get_contents("https://www.instagram.com/explore/tags/$tag/?__a=1"));
$nodes = $answer->tag->media->nodes;

foreach($nodes as $image)
{
	if($image->is_video) continue;
	echo "<div style='height: 320px; padding-left: 340px; margin-bottom: 20px; background: url({$image->thumbnail_src}) no-repeat; background-position: left center; background-size: auto 100%;'>";
	$imageInfo = json_decode(file_get_contents("https://www.instagram.com/p/{$image->code}/?tagged=$tag&__a=1"))->graphql->shortcode_media;
	echo "<h3>". $imageInfo->owner->full_name ." (<a href='https://www.instagram.com/{$imageInfo->owner->username}/' target='_blank'>{$imageInfo->owner->username}</a>)</h3>";
	echo "<p>$image->caption</p>";
	echo "</div>";
}
?>