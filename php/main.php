<?php
session_start();
include_once("SimpleApp.php");

function template_top()
{
	?>
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset='utf-8' />
			<meta name="viewport" content="width=device-width, initial-scale=1" />
			<title>ЧМ2018</title>
			<!--link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" /-->
			<link rel="stylesheet" href="/css/jquery-ui.min.css">
			<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" /-->
			<link rel="stylesheet" href="/css/bootstrap.min.css" />
			<link rel="stylesheet" href="/fonts/Gilroy/Gilroy.css" />
			<link rel="stylesheet" href="/css/main.css" />
			<link rel="stylesheet" href="/css/CrossesMessager.css" />
			<!--link rel="stylesheet" href="/css/desktop.css" /-->
			<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			<link rel="image_src" href="http://www.нашчм.рф/img/x-image.jpg" />
			<meta property="og:image" content="http://www.нашчм.рф/img/x-image.jpg" />
		</head>
		<body>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container">
					<div class="navbar-header col-sm-2">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_menu" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand logo" href="/"></a>
					</div>

					<div class="collapse navbar-collapse" id="main_menu">
						<ul class="nav navbar-nav">
							<li class='col-sm-2 logo2 hidden-xs'>
								<div>Всероссийская команда</div>
								<div>поддержки крупных</div>
								<div>спортивных событий</div>
							</li>
							<li class="menu"><a href="/" data-anchor='.screen02'>О проекте</a></li>
							<li class='menu'><a href="/" data-anchor='.screen07'>Программы</a></li>
							<li class='menu'><a href="/reports.php">Отчеты</a></li>
							<li class='menu'><a href="/live.php">ЧМ Live!</a></li>
							<li class='menu'><a href="/contacts.php">Контакты</a></li>
							<li class='menu_social'><a href='https://www.facebook.com/groups/MyWorldCup2018/' target='_blank'><?=file_get_contents("img/socials/fb.svg")?></a></li>
							<li class='menu_social'><a href='https://www.instagram.com/myworldcup2018' target='_blank'><?=file_get_contents("img/socials/inst.svg")?></a></li>
							<li class='menu_social'><a href='https://vk.com/myworldcup2018' target='_blank'><?=file_get_contents("img/socials/vk.svg")?></a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
			<div id='main_body'>
	<?php
}


function template_bottom()
{
	?>
				<footer>
					<div class='container'>
						<div class='row'>
							<a href='/' class='logo' data-anchor='.screen01'></a>
							<div class='menu'>
								<a href="/" data-anchor='.screen02'>О проекте</a>
								<a href="/" data-anchor='.screen07'>Программы</a>
							</div>
							<div class='menu'>
								<a href="/reports.php" data-anchor=''>Отчеты</a>
								<a href="/live.php" data-anchor=''>ЧМ Live!</a>
								<a href="/contacts.php" data-anchor=''>Контакты</a>
							</div>
							<div class='contact'>
								<a href='https://www.facebook.com/groups/MyWorldCup2018/' target='_blank'><?=file_get_contents("img/socials/fb.svg")?></a>
								<a href='https://www.instagram.com/myworldcup2018' target='_blank'><?=file_get_contents("img/socials/inst.svg")?></a>
								<a href='https://vk.com/myworldcup2018' target='_blank'><?=file_get_contents("img/socials/vk.svg")?></a>
							</div>
							<a class='sd'></a>
						</div>
					</div>
				</footer>
			</div>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
			<script src="/js/jquery.stellar.min.js"></script>
			<script src="/js/parallax.min.js"></script>
			<script src="/js/CrossesMessager.js"></script>
			<script src="/js/jquery.formSubmit.js"></script>
			<script src='/js/main.js'></script>

			<!— Yandex.Metrika counter —>
			<script type="text/javascript" >
			(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
			try {
			w.yaCounter47521096 = new Ya.Metrika2({
			id:47521096,
			clickmap:true,
			trackLinks:true,
			accurateTrackBounce:true
			});
			} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = "https://mc.yandex.ru/metrika/tag.js";

			if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
			})(document, window, "yandex_metrika_callbacks2");
			</script>
			<noscript><div><img src="https://mc.yandex.ru/watch/47521096" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
			<!— /Yandex.Metrika counter —>
			
		</body>
	</html>
	<?php
}
?>