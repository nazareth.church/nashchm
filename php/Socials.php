<?php
class Socials{
	const
		STATUS_WAITING = 0,
		STATUS_CONFIRMED = 1,
		STATUS_UNCONFIRMED = 2;
	
	private function __construct($data)
	{
		foreach($data as $key => $value)
		{
			$this->{$key} = $value;
		}
	}
	
	public function update($data)
	{
		foreach($data as $key => $value)
		{
			if(!isset($this->{$key}) || $this->{$key} == $value) continue;
			App::db("UPDATE `socials` SET `$key` = '".App::db()->real_escape_string($value)."' WHERE `ai` = {$this->ai}");
			if(!App::db()->error) $this->{$key} = $value;
		}
	}

	public function like($method = "get", $userKey = null)
	{
		if($userKey === null)
		{
			switch($method)
			{
			case "get":
				return App::db("SELECT * FROM `socials_like` WHERE `post_ai` = {$this->ai} AND `liked`")->num_rows;
				break;
			case "empty":
				App::db("UPDATE `socials_like` SET `liked` = 0 WHERE `post_ai` = {$this->ai}");
			}
		}
		else
		{
			$userKey = App::db()->real_escape_string($userKey);
			if(!App::db("SELECT * FROM `socials_like` WHERE `post_ai` = {$this->ai} AND `user` = '$userKey'")->num_rows)
			{
				App::db("INSERT INTO `socials_like` SET `post_ai` = {$this->ai}, `user` = '$userKey'");
			}
			switch($method)
			{
			case "get":
				return App::db("SELECT `liked` FROM `socials_like` WHERE `post_ai` = {$this->ai} AND `user` = '$userKey'")->fetch_assoc()["liked"] && true;
				break;
			case "toggle":
				App::db("UPDATE `socials_like` SET `liked` = NOT(`liked`) WHERE `post_ai` = {$this->ai} AND `user` = '$userKey'");
				break;
			case "on":
				App::db("UPDATE `socials_like` SET `liked` = 1 WHERE `post_ai` = {$this->ai} AND `user` = '$userKey'");
				break;
			case "off":
				App::db("UPDATE `socials_like` SET `liked` = 0 WHERE `post_ai` = {$this->ai} AND `user` = '$userKey'");
				break;
			}
		}
	}
	
	public static function get($options, $limit = 1)
	{
		if($limit <= 0) $limit = "";
		else $limit = "LIMIT $limit";
		$where = static::arrayToSql("WHERE", $options);
		
		$data = App::db("SELECT * FROM `socials` $where $limit");
		if($limit == "LIMIT 1")
		{
			return (($data = $data->fetch_assoc()) ?
				new static($data) :
				null
			);
		}
		else
		{
			$ret = [];
			while($next = $data->fetch_assoc())
			{
				$ret[] = new static($next);
			}
			return $ret;
		}
	}
	
	public static function getMany($limit = null, $status = null, $from = null, &$last = null, $userKey = null)
	{
		$ret = [];
		$where = "WHERE 1";
		if($limit != null) $limitStr = "LIMIT " . (1*$limit + 1);
		if($from != null) $where .= " AND CONCAT(`date`, `ai`) < '" . App::db()->real_escape_string($from) . "'";
		if($status != null) $where .= " AND `status` = " . ($status * 1);
		$data = App::db("SELECT * FROM `socials` $where ORDER BY `date` DESC, `ai` $limitStr");
		while($next = $data->fetch_assoc())
		{
			if($limit && count($ret) >= $limit)
			{
				$last = $ret[$limit - 1]->date . $ret[$limit - 1]->ai;
				return $ret;
			}
			$next = $ret[] = new static($next);
			$next->likes = $next->like();
			if($userKey !== null) $next->liked = $next->like("get", $userKey);
		}
		$last = "end";
		return $ret;
	}
	
	public static function add($data)
	{
		//die('{"success": 0, "message": "' . "INSERT INTO `socials` " . static::arrayToSql(["sql" => "SET", "concater" => ","], $data) . '"}');
		App::db("INSERT INTO `socials` " . static::arrayToSql(["sql" => "SET", "concater" => ","], $data));
		return static::get(["ai" => App::db()->insert_id]);
	}
	
	public static function arrayToSql($sql, $array)
	{
		if(count($array) < 1) return "";
		$def = [
			"operand" => "=",
			"concater" => "AND"
		];
		if(is_array($sql))
		{
			$def = array_merge($def, $sql);
			$sql = $def["sql"];
		}
		
		$concater = "";
		foreach($array as $field => $value)
		{
			$operand = $def["operand"];
			
			if(!is_array($value))
			{
				$sql .= " $concater `$field` $operand '" . App::db()->real_escape_string($value) . "'";
				$concater = $def["concater"];
				continue;
			}
			
			if(isset($value["operand"])) $operand = $value["operand"];
			if(isset($value["concater"])) $concater = $value["concater"];
			switch($value["type"])
			{
			case "processed":
				$sql .= " $concater `$field` $value[value]";
				break;
			default:
				
			}
			$concater = $def["concater"];
		}
		
		return $sql;
	}
}

?>