<?php
if(!preg_match("/files\/(.+)/", $_SERVER["REQUEST_URI"], $matches))
{
	include_once("../php/main.php");
	template_top();
	echo "<div class='container'><h3 class='c-pink'>404 Not Found</h3><p>Не удалось обработать ваш запрос.</p></div>";
	template_bottom();
	die();
}

$files = [
	"material-1.zip" => [
		"name" => 'Программа Спортивные турниры.zip',
		"type" => "application/x-7z-compressed",
	],
	"material-2.zip" => [
		"name" => 'Программа Спортивные клубы.zip',
		"type" => "application/x-7z-compressed",
	],
	"material-3.zip" => [
		"name" => 'Программа Спортивные фестивали.zip',
		"type" => "application/x-7z-compressed",
	],
	"material-4.zip" => [
		"name" => 'Программа Спортивные лагеря.zip',
		"type" => "application/x-7z-compressed",
	],
	"material-5.zip" => [
		"name" => 'Руководство по фирменному стилю.zip',
		"type" => "application/x-7z-compressed",
	],
	"material-6.zip" => [
		"name" => 'Маркетинг-кит На старт, внимание, марш.zip',
		"type" => "application/x-7z-compressed",
	],
];

$file = $matches[1];
if(!isset($files[$file]) || !is_file($file))
{
	include_once("../php/main.php");
	template_top();
	echo "<div class='container'><h3 class='c-pink'>404 Not Found</h3><p>Файл не найден. Возможно он ещё не был загружен, в таком случае подождите некоторое время и ссылка заработает.</p></div>";
	template_bottom();
	die();
}

header('X-SendFile: ' . __DIR__ . '/' . $file);
header('Content-Type: ' . $files[$file]["type"]);
header("Content-Disposition: attachment; filename=\"" . $files[$file]["name"] . "\"");
