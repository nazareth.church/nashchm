<?php
include_once("php/main.php");

template_top();
?>
<link rel="stylesheet" href="/css/contacts.css" />

<section class='screen01'>
    <div class='container'>
        <?=file_get_contents("img/sc01-back01.svg")?>
        <?=file_get_contents("img/sc01-back02.svg")?>
        <div class='row'>
            <div class='col-xs-offset-2 col-xs-8 text-center'>
                <img class='title' src='/img/contacts-header.svg' alt='МЫ ВСЕГДА НА СВЯЗИ И ОТКРЫТЫ ДЛЯ ОБЩЕНИЯ' />
            </div>
        </div>
    </div>
</section>

<section class='screen02'>
    <div class='container'>
        <div class='row'>
            <div class='col-sm-10 col-sm-offset-1 text-center'>
				<p class='txt01'>Если у вас есть предложения или вы хотите стать партнером движения «На старт, внимание, марш!» - напишите нам и мы свяжемся с вами в ближайшее время.</p>
                <form class='form1' action='/form.php' method='POST'>
                    <input type='hidden' name='formname' value='contact'>
                    <input name='FIO' placeholder='Ваше имя*' required />
                    <input name='address' placeholder='Город' />
                    <input name='email' type='email' placeholder='E-mail*' required />
                    <input name='phone' type='tel' placeholder='Контактный телефон' />
                    <textarea name='report' placeholder='Дополнительная информация*' required ></textarea>
                    <button class='c-green'>Получить</button>
                </form>
            </div>
        </div>
    </div>
</section>

<?php
template_bottom()
?>

<script src='/js/contacts.js'></script>