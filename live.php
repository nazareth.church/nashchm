<?php
include_once("php/main.php");

template_top();
?>
<link rel="stylesheet" href="/css/live.css" />

<section class='screen01'>
	<div class='container'>
		<?=file_get_contents("img/sc01-back01.svg")?>
		<?=file_get_contents("img/sc01-back02.svg")?>
		<div class='row'>
			<div class='col-xs-offset-2 col-xs-8 text-center'>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 716.43 347.85" style='overflow: visible;'>
					<text style='font-family:Gilroy-ExtraBold, Gilroy-Light;font-weight:700;font-size:149px;fill:#c3006f;' transform="translate(0 130.99)">
						ХРОНИКА
					</text>
					<text style='font-family:Gilroy-ExtraBold, Gilroy-Light;font-weight:700;font-size:96px;fill:#a0be37;' transform="translate(0 244.41)">
						ЧЕМПИОНАТОВ
					</text>
					<text style='font-family:Gilroy-ExtraBold, Gilroy-Light;font-weight:700;font-size:64.3px;fill:#e3a631;' transform="translate(0 330.05)">
						В РЕАЛЬНОМ ВРЕМЕНИ
					</text>
				</svg>
				<h2><a href='/' class='c-green' data-anchor='.screen03'>#НСВМ</a></h2>
			</div>
		</div>
	</div>
</section>

<section class='screen02'>
	<div class='container'>
		<div class='row'>
			<div class='col-sm-10 col-sm-offset-1 text-center'>
				<h4 class='c-pink'>Как сюда попасть?</h4>
				<p>Все очень просто! Публикуйте фотографии с места проведения ваших мероприятий в социальных сетях (Вконтакте, Instagram и Facebook) с нашим хэштегом #НСВМ и ваши фото попадут в общую ленту движения «На старт, внимание, марш!»</p>
				<p><b>Мы создадим легендарную хронику вместе!</b></p>
			</div>
		</div>
	</div>
</section>

<section class='screen03'>
	<div class='container-fluid'>
		<div class='row live-container'> </div>
		<a class='c-pink live-loader' href='/live.php' data-anchor='.live-container .live:last .description' onclick='loadContent()'>Загрузить предыдущие фото</a>
	</div>
</section>

<?php
template_bottom()
?>

<script src='/js/live.js'></script>