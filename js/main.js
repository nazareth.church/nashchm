'use strict';

$(function(){
	window.alert = new CrossesMessager();

	window.$main_scrolling = ($(window).width() >= 768 ? $("#main_body") : $("html, body"));
	
	if($(window).width() >= 768)
	{
		$($main_scrolling).stellar({
			horizontalScrolling: false,
			positionProperty: 'position',
			hideDistantElements: true,
			responsive: true,
		});
	}
	
	(function(){
		var goto = function($obj)
			{
				$($main_scrolling).animate({
					scrollTop: $obj.offset().top + ($main_scrolling.selector != "html, body") * ($main_scrolling.scrollTop() - $main_scrolling.offset().top)
				}, 500);
			},
			action = location.pathname + location.search,
			hash = /^#?(.*)$/.exec(location.hash)[1],
			$obj = $(hash);

		if($obj.length) goto($obj);

		$("[data-anchor]").on("click", function(event){
			event.preventDefault();

			$obj = $($(this).data("anchor"));
			if ($(this).attr("href") != action || !$obj.length)
			{
				location.href = $(this).attr("href") + "#" + $(this).data("anchor");
			}
			else
			{
				goto($obj);
			}
			return false;
		});
	})();
	
	
	window.wheelAction = (function(){
		var elements = [];
		
		$($main_scrolling).on("scroll resize load", function(){
			var top = 0,
				bottom = window.innerHeight;
			
			for(var i = elements.length - 1; i >= 0; --i)
			{
				var element = elements[i],
					state = 1;
				
				for(var j = element.bodys.length - 1; j >= 0; --j)
				{
					var body = element.bodys[j];
					if(!body.getBoundingClientRect) continue;
					if(body.getBoundingClientRect().top < bottom && body.getBoundingClientRect().bottom > top)
					{
						state = 0;
						break;
					}
				}
				
				if(element.state != state)
				{
					element.state = state;
					element.callback[state].call(element.bodys.length > 1 ? element.bodys : element.bodys[0]);
				}
			}
		});
		
		return function(element, onVisible, onInvisible)
		{
			if(!onVisible) onVisible = function(){};
			if(!onInvisible) onInvisible = function(){};
			if(element.length === undefined) element = [element];
			elements.push({
				bodys: element,
				state: 1,
				callback: [onVisible, onInvisible]
			});
		}
	})();
	
	$("[data-select-wrapper]").each(function(){
		$("<img src='/img/arrow.png' alt='' />").appendTo(this);
		
		var $select = $(this).click(function(e){
				if(e.target == $select[0]) $select.toggleClass("focus");
			}),
			input = $("<input type='hidden' name='" + $select.data("select-wrapper") + "' value='' class='" + ($select[0].hasAttribute("required") ? "required" : "") + "' />").appendTo($select),
			wrapper = $("<span class='placeholder'>" + $select.data("placeholder") + "</span>").appendTo($select),
			$options = $select.find(".options span");
			
			
		$select.blur(function(){
			$select.removeClass("focus");
		});
		
		$select[0].__defineGetter__("value", function(){return input.val();});
		$select[0].__defineSetter__("value", function(value){
			for(var i = $options.length - 1; i >= 0; --i)
			{
				if($($options[i]).data("value") != value && $options[i].innerHTML != value) continue;
				wrapper.html($options[i].innerHTML).removeClass("placeholder");
				break;
			}
			input.val(value);
			$select.trigger("change").blur();
		});
		
		$options.on("click", function(){
			var value = $(this).attr("data-value");
			if(!value) value = $(this).html();
			$select.val(value);
		});
	});
	
	//parallax on screen01
	(function(){
		var $container;
		for(var i in {".screen01 .back01": 1, ".screen01 .back02": 2})
		{
			$container = $(i);
			if($container.length){
				var parallax = new Parallax($container[0]);
			}
		}
	})();
});