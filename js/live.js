$(function(){
	(function(){
		var postTemplate = function(data){
			return "<div class='col-md-3 col-sm-6'> \
						<div class='live' tabindex='0'> \
							<img src='"+data.image+"' alt='"+data.type+"' /> \
							<p class='info'>"+data.info+"</p> \
							<div class='description'> \
								<div class='like "+(data.liked ? "liked" : "")+"'>"+data.likes+"</div> \
								<span class='date'>"+data.date+" г.</span> \
								<a href='"+data.author_url+"' target='_blank'>Перейти в профиль автора</a> \
							</div> \
						</div> \
					</div>";
		}
		var lastPost = null;
		window.loadContent = function(callback)
		{
			if(lastPost == "end") return;
			$.post("/php/socApi.php", {getContent: 1, lastPost: lastPost}, function(data){
				if(!data.success) return alert(data.message);
				$.each(data.posts, function(){
					$(postTemplate(this))
						.appendTo(".live-container")
						.find(".like").click((function(ai){
							return function(){
								var $self = $(this).addClass("disabled");
								$.post("/php/socApi.php", {like: ai}, function (data) {
									$self.removeClass("disabled");
									if(!data.success) return alert(data.message, 5000, "error");
									$self
										.html(data.likes)
										.toggleClass("liked", data.liked);
								}, "json");
							}
						})(this.ai));
				});
				if((lastPost = data.last) == "end") $(".live-loader").css("display", "none");
				if(typeof callback == "function") callback();
			}, "json");
		}
	})();
	
	$(".menu>[href='/live.php']").parent().addClass("active");
	loadContent();
});