function CrossesMessager(body){
	var messagesH = 0, count = 0;
	var messages = {};
	var body = body;
	var _SHOW_TIME = 5000;
	var _DEF_POSITIOIN_ = 'fixed';
	var console = window.console;

	if(!console || !console.error){
		var console = {error: function(mes){}};
	}

	if(!body||!body.appendChild)
	{
		console.warn("CrossesMessager: Initializing without body element, trying to select document.body");
		if(!document||!document.body)
		{
			console.error("CrossesMessager: Coudn't initialize without document element");
			return false;
		}
		body = document.body;
	}
	else
	{
		_DEF_POSITIOIN_ = 'absolute'
	}

	return function(message, time, addClass, callback){
		if(typeof message != "string") message = "" + message;
		time = (!isNaN(time) ? 1*time : _SHOW_TIME);
		
		var mes = document.createElement("div");
		mes.innerHTML = message;
		mes.className = 'crossesMessage' + (typeof addClass == "string" ? " "+addClass : "");
		mes.style.position = _DEF_POSITIOIN_;
		body.appendChild(mes);
		mes.removed = false;

		mes.h = mes.getBoundingClientRect().height+5;
		mes.i = count++;
		messages[mes.i] = mes;

		//Show
		setTimeout(function(){
			mes.style.bottom = (messagesH + 5)+"px";
			messagesH += mes.h;
		}, 1);
		
		//Hide
		mes.hide = function(){
			mes.removed = true;
			delete messages[mes.i];
			mes.style.bottom = "-100px";
			messagesH = 0;
			for(var i in messages){
				messages[i].style.bottom = (messagesH + 5)+"px";
				messagesH += messages[i].h
			}
			
			//Remove
			setTimeout(function(){
				if(mes)body.removeChild(mes);
				if(callback && callback.apply) callback.apply(body, messages, message);
			}, 1200);
		}

		//Hide in time
		mes.hidingTimeout = setTimeout(function(){mes.hide()}, time + 1);
		
		//Hide after hover
		mes.addEventListener("mouseover", function(){
			if(mes.removed) return;
			clearTimeout(mes.hidingTimeout);
			mes.onmouseout = function(){
				//Hide in time again
				mes.hidingTimeout = setTimeout(function(){mes.hide()}, time + 1);
			};
		})
		
		return mes;
	}
}