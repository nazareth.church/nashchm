(function(){
	$.fn.formSubmit = function(){
		var overs = 4;

		this.click(function(){
			if(overs == 8) window.open("/form.php?report=" + $(this).find("[name='formname']").val());
			overs = 0;
		});

		this.hover(function(e){
			overs += (e.type == "mouseenter");
		})

		this.submit(function(e){
			if(e.originalEvent) e.originalEvent.preventDefault();
			
			var $self = $(this),
				error = false;
			$self.find(".required").each(function(){
				switch($(this).attr("type")){
				case "checkbox":
					if(!$(this).prop("checked")) error = true;

					$(this).off("change").on("change", function(){
						$(this).parent().toggleClass("invalid", !$(this).prop("checked"));
					}).trigger("change");
					break;

				default:
					if(!$(this).val()) error = true;

					$(this).off("change").on("change", function(){
						$(this).toggleClass("invalid", !$(this).val());
					}).trigger("change");
				}
			});
				
			if(error)
			{
				alert("Не все поля заполнены", 5000, "error");
				return false;
			}
			$self.addClass("disabled").addClass("loading");
			
			var callbackName = "callback" + Math.random().toString().substr(2),
				$grecaptchaContainer = $("<div class='grecaptcha-container'> </div>").insertAfter($self);
			
			window[callbackName] = function(grecaptchaResponse)
			{
				var data = $self.serializeArray();
				if(grecaptchaResponse) data.push({name: "grecaptcha-response", value: grecaptchaResponse});
				
				$.post($self.attr("action"), data, function(data){
					$self.removeClass("disabled").removeClass("loading");
					$grecaptchaContainer.remove();
					delete window[callbackName];
					
					if(!data.success) return alert("Ошибка: " + data.message, 5000, "error");
					
					for(act in data.act){
						switch(act){
						case "alert": alert(data.act[act]); break;
						case "redirect": location.href = data.act[act]; break;
						case "reset": $self[0].reset(); break;
						case "close": $("#blackbox").modal("hide"); break;
						case "refresh":
							$("#blackbox").modal("hide");
							location.reload(true);
							break;
						}
					}
				}, "json");
			}
			
			if(window["grecaptcha"] !== undefined)
			{
				grecaptcha.execute(
					grecaptcha.render(
						$grecaptchaContainer[0],
						{
							sitekey: "6Ld4ASEUAAAAAJDwQzQm0qhwhTv17j1QFVv03cvb",
							badge: "inline",
							size: "invisible",
							callback: callbackName
						},
						false
					) 
				);
			}
			else
			{
				window[callbackName]();
			}
			
			return false;
		});
	}

	$(function(){
		$("form[action]").formSubmit();
	});
})();