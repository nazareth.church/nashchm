$(function(){	
	$(".showType").on("click, change", function(){
		$(".post.status" + this.value).css("display", ($(this).is(":checked") ? "block" : "none"));
	});
	
	$(".stateChanger").on("change", function(){
		var $self = $(this),
			status = $self.val();
		$self.attr("disabled", "disabled");
		$.post("/php/socApi.php", {updatePost: $self.data("ai"), value: status}, function(data){
			$self.attr("disabled", false);
			if(!data.success) return alert(data.message);
			$self.parent().removeClass("status0, status1, status2").addClass("status" + status).css("display", ($(".showType[value='"+status+"']").is(":checked") ? "block" : "none"))
		}, "json");
	});
	
	$(".menu>[href='/live.php']").parent().addClass("active");
	$(".showType[value='0']").attr("checked", "checked").trigger("change");
});

function update()
{
	var $container = $(this).parents(".container").css({
		opacity: 0.5,
		"pointer-events": "none"
	});
	
	$.post("/php/socApi.php", {update: 1}, function(data){
		if(!data.success) return alert(data.message);
		location.href = location.href;
		location.reload();
	}, "json");
}